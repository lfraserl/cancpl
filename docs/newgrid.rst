Creating a restart for a new ocean grid
=======================================

This document describes the bootstrap method used to generate the initial conditions for the AGCM when introducing a new
NEMO configuration (i.e. anthing other than ORCA1, ORCA025, and eORCA1). This document assumes that the user has a
general knowledge of running CanESM5.

1. Ensure that you have a working NEMO, OMIP-style simulation that can at least run a few timesteps.
2. ``save`` all the necessary input fields and and update the CanESM sequencing, perhaps specifying a new NEMO
   configuration, and CanESM5 runmode
3. Create a new CanESM5 run in the typical way
4. Modify canesm.cfg with the following
   - Update ``nemo_config`` to the new configuration
   - Update the runmode to ``CanESM-init``
   - ``debug=on`` to make sure the run directory is retained when the model crashes
5. Run the model in the normal way. The model will likely crash at the end of the first timestep. This is expected
   because the ocean and atmosphere do not agree on the land mask.
6. Navigate to the run directory under ``$CCRNTMP``
7. Copy the ``cpl_fland.nc`` file in the directory to a temporary scratch directory
8. From the ``cpl_fland.nc`` file create a new file that contains the variable ``SRCF`` which is fraction of ocean in each
   grid cell.

   a. Use python, ncks, etc. to calculate ``1-FLND`` and create a new netCDF file with the variable ``SRCF``
   b. Use ``nc2ccc`` to convert this file to a CCCma-style file

9. This file can then be passed to Vivek to create a new river routing file
10. ``save`` both the new ``SRCF`` and river routing file
11. Update ``CCCma_tools/cccjob_dir/lib/jobdefs/canesm_cgcm_jobdef``

    a. Change ``maskfile`` to the saved file with ``SRCF``
    b. Change ``target`` to the new river routing file

12. After all these changes, run the model again. This will run for a year and create a new useable restart.





