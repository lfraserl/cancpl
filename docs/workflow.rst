Workflow
========
The programmatic workflow of CanCPL behaves differently depending on the type of configuration of CanESM. The
following provide a detailed explanation of each of the major run-types that use CanCPL. The AGCM always uses the
coupler to receive its boundary conditions regardless of whether it is being run in an atmosphere-only mode or coupled to
a fully dynamic ocean/ice model.

(Non-) Event-based timing
~~~~~~~~~~~~~~~~~~~~~~~~~
Much of the code base of CanCPL seems to be comprised of code related to alarms and events, both terms and concpets
used in Earth System Model Framework. In practice, however the lead task of each model waits for the reception
of a 'message' from the coupler to initiate a pre-defined list of 'events' that define the receipt and sending of
fields needed to drive the ocean and atmosphere. The following are brief descriptions
of each of these terms

Event
  Defined in the code using ``event_t`` type in ``com_cpl.F90``. Stores the name of the sending and receiving component,
  variable name in each component, coupling frequencing, regridding options, and id of alarm associated with the event.

  ``MSG_ocn``
    Sends an integer array from the ocean to the atmosphere (and coupler) with the following definitions by index of the
    array

    1. If ``1`` the ocean has not finished, ``0`` otherwise
    2. The calendar year from the ocean
    3. The day of the year from the ocean
    4. The second of the day

  ``MSG_atm``
    An integer array representing information from the atmosphere

    1. Day of year
    2. Number of days since start of simulation ``NDAYS``
    3. Present month ``MONTH``
    4. Set to DONE if AGCM is at the end of a month
    5. Starting timestep of the model ``KSTART``
    6. Length of the timestep ``DELT``
    7. Current timestep of the atmosphere ``KOUNT``

  ``Data``
    A 2-dimensional array representing surface fields or fluxes. These could potentially be regridded and remapped
    between the grids

Each transfer of fields between components and in both directions has an associated array of events defined and added to
the main event list in ``com_cpl.F90``:

- ``add_events_cpl_to_nemo``
- ``add_events_nemo_to_cpl``
- ``add_events_cpl_to_atm``
- ``add_events_atm_to_cpl``

The order in which these four subroutines are called define the overall ordering of the events and determine whether a
'serial' or 'parallel' coupling strategy is used. In a serial coupling case, within a timestep of the coupled model,
one component (in the current definition, the atmosphere) takes its timestep first and then passes fields to the ocean.
This is akin to using a forward-backward strategy for the coupling as the second called component (the ocean) receives
fields from the first component (the atmosphere) at the end of its timestep. In a parallel strategy, both the
atmosphere and ocean take their timestep simultaneously and then fields are exchanged.  Note CanESM5, uses the parallel
strategy which may be susceptible to instabilities at high resolutions.

Processing an event
-------------------
As the coupler clock is advanced in the main loop of the coupler, the order of events listed in `init_events_part1`
is processed with most calls being blocking. When the correct ``OCN_mesg`` or ``ATM_mesg`` is received, these
events are processed. ``execute_event`` handles the initial processing before
being passed to ``transfer_data`` to actually receive and send data from the correct model components. At this stage,
if the field has an associated ``regridding_id``, the field will also be remapped. In addition, after exiting
``transfer_data`` if the event type is a ``OCN_mesg`` or ``ATM_mesg``, the content of the message is stored in the
coupler.

Atmosphere/Ocean/Ice
~~~~~~~~~~~~~~~~~~~~
CanESM5 represents a fully-coupled configuration where the AGCM (land/atmosphere) is fully coupled to NEMO/LIM2
(ice/ocean). The AGCM-CanCPL interaction in the fully-coupled configuration is essentially the same as
atmosphere-only, data ice/ocean configurations (e.g. AMIP). This is not true of data atmosphere, ice-ocean-only
simulations using NEMO/LIM2 (which does not currently use the coupler).

The ocean is largely driven by fluxes calculated in the AGCM's parameterizations of the atmospheric boundary layer
(averaged over the coupling frequency of the AGCM) (e.g. average shortwave radiaton over 3 hours) whereas the
atmosphere is driven primarily by instantaneous vlaues of the prognostic state variables (e.g. sea-surface
temperature at the end of a coupling call). The primary exception to this is CO\ :sub:`2`-- the atmosphere passes
the instantaneous, surface CO\ :sub:`2` concentrations and the ocean calculates the ocean/atmosphere flux.

The fluxes are calculated on the atmospheric grid which is coarser than the ocean grid. In CanESM5, the heat and water
fluxes are calculated using a conservative remapping scheme, which can result in 'blocky' structures in various ocean
quantities. This was particularly evident in the ``p1`` variant of the model in which the sharp transitions between AGCM
grid cells of wind stress resulted in spurious divergence of oceanic currents. The ``p2`` variant instead used a
bilinear remapping scheme.

Atmosphere only
~~~~~~~~~~~~~~~
AGCM runs with no dynamic ice/ocean model (also referred to as AMIP-mode) read in boundary conditions from files.
However from the point of view of the AGCM, the communication framework is unchanged between AMIP and fully-coupled
runs.

From a configuration standpoint, two switches are required to run CanCPL in AMIP mode: a namelist parameter
``atm_forcing_from_file = .true.`` and the CPP macro ``use_NEMO`` must be undefined.

.. note::
  It may be possible to have ``atm_forcing_from_file = .true.`` and with ``use_NEMO`` defined. In this case, the
  ``atm_forcing_from_file`` behavior *should* be retained (including ocean events), but this has not been
  confirmed.

The low level routines that coordinate the reading, time interpolation, and masking of the fields are in
``atm_f_data.F90``. Many of these were ported directly from code used in the AGCM code, notably ``read_spec_bc`` and ``PHSIL8``.

