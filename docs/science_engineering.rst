Open Research/Engineering questions
===================================
The coupling in CanESM5 largely works in that fluxes, scalar, and vector fields
are exchanged between the grids on the AGCM and the ocean. Work at other
modelling centers though suggest some broad improvements that could be made to
enhance the accuracy and stability of the coupling.

Examine the definition of 'SST'
-------------------------------
Sea surface temperature currently is defined as simply the the temperature of
the top layer of the ocean model (nominally the top 3m). Given the relatively
long coupling time between atmosphere and ocean (3 hours), the this might not
be the correct definition. The 'bulk' temperature of the ocean boundary layer
might be a more accurate in this case. This likely however would decrease the
surface temperature since temperatures are cooler below the surface layer.
It could also be argued, depending on the assumptions used in the AGCM boundary
layer scheme, that the SST might need to be extrapolated from the 3m temperature
to something more akin to the skin or surface temperatures.

This can be done relatively easily by averaging the `thetao` array in NEMO over
either a specified depth or the diagnosed boundary layer depth. In the
`O_SSTSST` field this diagnostic array can be sent instead.

Calculate surface stress from relative velocities
-------------------------------------------------
The momentum flux portion of the AGCM boundary layer scheme assumes that the
ocean is at rest. This is inaccurate in regions of the ocean where prevailing
winds are aligned with ocean currents. In some regions, this can lead to an
enhancement in wind stress when the ocean current flows opposite to the AGCM
winds or a reduction in when stress when ocean currents are in the same
diretion as the wind. Papers have shown that taking relative velocities into
an account can lead to an overall reduction in wind energy input into the ocean
by around 30%.

To achieve this in CanESM5, the boundary layer scheme needs to accept the
meridonal and zonal components of ocean velocity to calculate the relative
velocity. On the ocean side, the U, V velocities should be interpolated to the
T-point of the ocean's C-grid, the components rotated to true meridional/zonal,
and then interpolated onto the atmospheric grid.

Move to higher resolution grid for flux calculation
---------------------------------------------------
Currently, the AGCM calculates the fluxes on its own grid, which is about a
factor of 3 (or more) coarser than the ocean grid. This can result in 'blocky'
patterns of fields passed from the AGCM to NEMO. To alleviate this, bilinear
interpolation for fields which do not need to be conserved (e.g. wind stress)
is used and a second-order accurate conservation scheme is used for those that
must be conserved (i.e. freshwater and heat fluxes).

One solution is to move the calculation of fluxes onto the ocean grid. This
might entail moving the atmospheric boundary layer calculation (or potentially)
the entire AGCM physics package into the coupler. The scalar fields (e.g.
temperature, winds, humidity, etc.) could then be interpolated onto the ocean
grid.

A better solution, known as the 'exchange grid' approach has been used at GFDL.
An intermediate grid which is the superposition of the atmospheric and ocean
grids is created. Scalar fields are then remapped onto this intermediate grid
and fluxes are computed. Any extensive fields (e.g. fluxes) can then be
aggregated onto the component grid cell exactly. This approach is
documented in more detail `in a paper by Balaji <https://extranet.gfdl.noaa.gov/~vb/pdf/xgridpaper.pdf>`_.

Separate 'slow' and 'fast' processes
------------------------------------
CanESM5 uses a 3 hour coupling interval. While this time is sufficient for 'slow'
processes (like ice melting in response to a change in temperature), it may not
be accurate for 'fast' processes like thermodynamics (e.g. changing the surface
temperature of ice). To better address this, the coupling and boundary calculation
can be split into fast and slow processes. In addition to modifications, within
the timestepping within each of the components, a new order of event lists will
need to be defined within the coupler.

The current types of coupling algorithms may be only nominally stable and overly
damped. Recent work has examined the coupled boundary layer problem as a multi-rate
problem. Notably `Marti et al., 2021 <https://gmd.copernicus.org/articles/14/2959/2021/>`_.
use a Schwarz method to solve the system more accurately. A relatively approachable
overview of the coupling problem can be found in a `presentation given by Florian
LeMarie <https://hal.inria.fr/hal-03534143/file/Lemarie_USNCCM16_2021.pdf>`_.

Move to a new coupler
---------------------
The coupler in its current formulation is heavily dependent on the formulation
of the AGCM and NEMO. Additionally, the code contains a large amount of
development code that may not be used. Lastly, the code is relatively difficult
to parse. Refactoring this coupler would likely take about as much work as moving
to a new coupling system while still making the code fairly reliant on bespoke
development at the CCCma.

Community coupling frameworks (e.g. NUOPC from NCAR, OASIS from NEMO, and FMS
from GFDL) are available. By implementing one of these frameworks, some of the
above issues can be addressed more easily and also allow for research into
coupling strategies and numerical implementations to be shared among the
broader scientific community.

The NUOPC layer is an attractive solution for the CCCma especially because
NCAR has a strong mandate for community and modelling center interactions. Such an
implementation however would require some rewriting of the CanAM code and may
not be aligned with the design philosophy of the GEM dynamical core. More
specifically, each component essentially becomes a subroutine of the coupled
driver, with the driver handling the divvying of MPI tasks and communicators
and controlling the timestepping of the model. Initial discussions with RPN suggest
a that this would be a major change in the design philosophy for GEM.