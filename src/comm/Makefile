#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Create the coupler common library to be used by all programs that are coupled.
#
# Larry Solheim  ...Mar,2014
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#
# The following variables may be defined either in the invoking environment
# or on the make command line of the makefile that includes this file.
# These user supplied variables will affect the values of the variables defined above
# All these variables have default values.
#   modver  ...AGCM model version                                (default gcm18)
#   mpi     ...used to define the the update token "MPI"            (default on)
#   coupled ...used to define the the update token "COUPLED"        (default on)
#   cppdefs ...the name of a file containing cpp preprocessor directives
#              (default is CPP_I if that file exists in cwd, undef otherwise)
#   float1  ...used in the determination of the default compiler (default undef)
#              float1=on is the only value that will have any effect
#   openmp  ...used in the determination of the default compiler (default undef)
#              openmp=on is the only value that will have any effect
#   FC      ...the name of the fortran compiler
#              (default depends on other parameters such as float1, openmp, ...)
#   FFLAGS  ...fortran compiler options
#              (default depends on other parameters such as FC, float1, openmp, ...)
#   netcdf  ...used to set compiler options for using netcdf libraries (default on)
#   p5lib   ...used to set compiler options that force the use of a particular set
#              of libraries compatible with an old power5 system       (default off)
#   xlfqinitauto ...used to set compiler options that cause automatic arrays to be
#                   initialzed or not initialized                      (default on)
#                   Other compiler options are also affected by xlfqinitauto
#   xlfimp  ...used to keep or remove the "-qflttrap=imprecise" option from
#              xlf compiler command line options                       (default on)
#   xlflarge...used to add size load options for the xlf compiler      (default off)
#   libdiag ...used to link against the compiled library libLOSUB_diag.a, or variant
#              in addition to other standard compiled libraries        (default off)
#   show_dep_ccrnsrc ...when show_dep_ccrnsrc=on then a list of files from CCRNSRC
#              that are required for the current build will be written to stdout
#              and the program will then stop                          (default off)

# Define an "all" target before make_defs_cccma.mk is included below
.PHONY: all
all: cplcom

# Find all fortran source files in directories specified by SRC_DIRS
# These files will be assigned to the variable FSRC by make_defs_cccma.mk
# and this list will be ordered according to module and include dependenies
# (least dependent first).
# Only the basename of each file will appear in FSRC.
# make_defs_cccma.mk will also write dependency rules into the current makefile
# for any of the files in FSRC that have module or include dependencies
SRC_DIRS := .

# If FSRC is defined here then it is assumed to contain the list of source files
# for which dependencies are to be determined and SRC_DIRS is ignored.
# If defined here, file names in FSRC must contain any necessary directory components
# (relative or absolute) to be visible from the directory containing this makefile.
# After being processed by make_defs_cccma.mk FSRC will contain only the
# basename part for each file name in the list.
# FSRC := $(wildcard *.F90) $(wildcard ../lakes/lakes.F90) $(wildcard ../lakes/CanOM/*.f) ../drivers/cpl_main.F90
FSRC := $(wildcard *.F90) ../drivers/cpl_main.F90

ifeq ($(findstring regrid,$(MAKECMDGOALS)),regrid)
  # Build only remapper and regrid dependencies
  FSRC := $(wildcard *.F90) ../drivers/regrid.F90
endif

# Define commonly used variables
# make_defs_cccma.mk will define the following variables
#   MACH_TYPE    ...kernel type on invoking machine
#   MACH_NAME    ...name of invoking machine
#   LOCATION     ...physical location of invoking machine (e.g. cccma, cmc)
#   MODVER       ...the AGCM model version (e.g. gcm16, gcm17, ...)
#   UP2CPP_OPTS  ...command line options to be used with up2cpp
#   FC           ...the name of the fortran compiler (e.g. pgf90, mpxlf90, ...)
#   FFLAGS       ...fortran compiler options to use with FC
#   FIXED        ...FC option used to indicate fixed format fortran source
#   FREE         ...FC option used to indicate free format fortran source
#   LISTSRC      ...FC option used to produce a source listing at compile time
#   NETCDF_INCS  ...FC options used to locate netcdf includes
#   NETCDF_LIBS  ...FC options used to locate netcdf libraries
#   STAMP        ...a date/time string suitable for appending to file names
#   VPATH        ...initialized with CCCma source directories
include ../make_defs_cccma.mk

# After being processed in make_defs_cccma.mk
# $(FSRC) will contain the original file list but with any leading pathname component removed

# $(RAW_FSRC) will contain the same list as FSRC but any pathname information
# that is available will be retained

# $(ALL_FSRC) will contain files in FSRC as well as all files containing dependencies
# with any leading pathname component removed. These files will be sorted according to
# dependency with the least dependent files first in the list.

# To link against ESMF libs we need some ESMF specific defs (e.g. ESMF_F90LINKPATHS, ...)
# These are defined in make_defs_cccma.mk if possible

# Remove certain external files from the list of files in ALL_FSRC
ALL_FSRC := $(filter-out cpl_cppdefs cpl_main.F90 regrid.F90,$(ALL_FSRC))

$(info In cplcomm     FSRC = $(FSRC))
$(info In cplcomm RAW_FSRC = $(RAW_FSRC))
$(info In cplcomm ALL_FSRC = $(ALL_FSRC))

# Get a list of object files from ALL_FSRC
# Remove all .cdk files except xit2.cdk from the list of object files
INC_SRC := $(filter-out xit2.cdk,$(filter %.cdk,$(ALL_FSRC)))
OBJS := $(filter-out $(INC_SRC), $(ALL_FSRC))
OBJS := $(patsubst %.f90,%.o, $(patsubst %.F90,%.o, $(OBJS)))
OBJS := $(patsubst %.f,%.o,   $(patsubst %.F,%.o,   $(OBJS)))
OBJS := $(patsubst %.dk,%.o,  $(OBJS))
OBJS := $(patsubst %.cdk,%.o, $(OBJS))

$(info make comm: INC_SRC = $(INC_SRC))
$(info make comm: OBJS = $(OBJS))

# override is required here because the user may supply a value for INCS on the command line
override INCS := -I../drivers $(INCS)

# Define a location to install the library once compiled
install_dir := ../..

# Name of the coupler common library
COMM_LIB  := libcplcom.a

.PHONY: debug
debug:
	@echo "LIBS     = $(LIBS)"
	@echo "INCS     = $(INCS)"
	@echo "FSRC     = $(FSRC)"
	@echo "ALL_FSRC = $(ALL_FSRC)"
	@echo "OBJS     = $(OBJS)"

cplcom: $(OBJS)
	ar -r $(COMM_LIB) $(OBJS)

install: cplcom chmod_all
	[ -d $(install_dir) ] && cp $(COMM_LIB) *.mod $(install_dir)

chmod_all:
	@chmod a+r $(COMM_LIB) *.mod

regrid: $(OBJS)
	ar -r libregrid.a $(OBJS) && cp libregrid.a *.mod $(install_dir)

.PHONY: clean veryclean
clean:
	-rm -f xit2.F *.o *.lst *.mod

veryclean: clean
	-rm -f $(COMM_LIB)
